<?php 

/**
 * A simple module to manipulate images using PHP Imagick
 *
 * @package Imagick Module
 * @author Rob Reyes
 * @version 1.0
 * 
 * Usage:
 * 
 * @param \Imagick $img
 *  Image to manipulate
 * @param int $width
 *  New image width 
 * @param int $height
 *  New image height
 * @param string $directory
 *  Destination path of the manipulated image, omit backslash on the end of path
 * 
 * 
 */

// Check if imagick is installed
if( ! extension_loaded( 'imagick' ) )
{
    error_log( 'Imagick not detected on the server' );
    exit; // Exit if not detected
}

function process_image( $image, $width, $height, $directory )
{   
    // Init new Imagick instance
    $img = new Imagick( $image );

    // Get target ratio
    $ratio = $width / $height;

    // Get image properties first
    $img_prop = [
        'cur_width'     => $img->getImageWidth(),
        'cur_height'    => $img->getImageHeight(),
    ];

    // Get current ratio
    $cur_ratio = $img_prop['cur_width'] / $img_prop['cur_height'];

    // Check new image scale dimensions 
    if( $ratio > $cur_ratio ) 
    {
        $new_width = $width;
        $new_height = $width / $img_prop['cur_width'] * $img_prop['cur_height'];
        
        $crop_x = 0;
        $crop_y = intval( ( $new_height - $height ) / 2 );
    }

    else 
    {
        $new_width = $height / $img_prop['cur_height'] * $img_prop['cur_width'];
        $new_height = $height;

        $crop_x = intval( ( $new_width - $width ) / 2 );
        $crop_y = 0;
    }

    // Resize image first to fit dimensions
    $img->resizeImage( $new_width, $new_height, imagick::FILTER_LANCZOS, 0.8, true );

    // Then crop to desired dimensions 
    $img->cropImage( $width, $height, $crop_x, $crop_y );

    $img_file = pathinfo( $image );
    $img_new_filename = $img_file['filename']. '_' . $width . 'x' . $height . '.' .  $img_file['extension'];

    // File destination
    $file_dest = $directory . '/' . $img_new_filename;

    // Lastly write the image to directory
    $img->writeImage( $file_dest );

    return [ 'img_path' => $file_dest ];
}